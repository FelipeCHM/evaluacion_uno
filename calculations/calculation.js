const filesM = require('../filesmanagement/filesM');
//calcula promedio
let PROM = function(i) {
    filesM.getAllFileValues(i)
        .then(function(array) {
            if (!array) {
                console.log("No hay datos para calcular el promedio");
            }
            let sum = 0;
            let n = array.length;
            let minDate = array[0].fecha;
            let maxDate = array[0].fecha;
            for (const val of array) {
                sum += parseFloat(val.valor);
                if (val.fecha > maxDate) {
                    maxDate = val.fecha;
                }
                if (val.fecha < minDate) {
                    minDate = val.fecha;
                }
            }
            console.log('Promedio:' + array[0].nombre + ' : ' + parseFloat(sum / n).toFixed(2));
            console.log("Rango:" + new Date(minDate).toLocaleDateString() + ' - ' + new Date(maxDate).toLocaleDateString());

        }).catch(console.warn('ERROR En calcular PROM'));
};
//calcula minimo historico
let MIN = function(i) {
    filesM.getAllFileValues(i)
        .then(function(array) {
            let min = array[0].valor;
            let valDate = array[0].fecha;
            let minDate = array[0].fecha;
            let maxDate = array[0].fecha;
            for (const val of array) {
                if (min > val.valor) {
                    min = val.valor;
                    valDate = val.fecha;
                }
                if (val.fecha > maxDate) {
                    maxDate = val.fecha;
                }
                if (val.fecha < minDate) {
                    minDate = val.fecha;
                }
            }
            console.log('Minimo histórico: ' + array[0].nombre + ' a ' + min + ' el ' + new Date(valDate).toLocaleDateString());
            console.log("Rango: " + new Date(minDate).toLocaleDateString() + ' - ' + new Date(maxDate).toLocaleDateString());
        }).catch(console.warn('ERROR en calcular Min'));
};
//calcula maximo historico
let MAX = function(i) {
    filesM.getAllFileValues(i)
        .then(function(array) {
            let max = array[0].valor;
            let valDate = array[0].fecha;
            let minDate = array[0].fecha;
            let maxDate = array[0].fecha;
            for (const val of array) {
                if (val.valor > max) {
                    max = val.valor;
                    valDate = val.fecha;
                }
                if (val.fecha > maxDate) {
                    maxDate = val.fecha;
                }
                if (val.fecha < minDate) {
                    minDate = val.fecha;
                }
            }
            console.log('Máximo histórico: ' + array[0].nombre + ' a ' + max + ' el ' + new Date(valDate).toLocaleDateString());
            console.log("Rango: " + new Date(minDate).toLocaleDateString() + ' - ' + new Date(maxDate).toLocaleDateString());
        }).catch(console.warn('ERROR en calcular MAX'));
};


module.exports = {
    PROM,
    MIN,
    MAX
};