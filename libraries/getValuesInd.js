const filesM = require('../filesmanagement/filesM');
const fetch = require('node-fetch');


const GETJSON = function(resp) { return resp.json(); };

const STATUS = function(resp) {
    if (resp.status >= 200 && resp.status < 300) {
        return Promise.resolve(resp);
    }
    return Promise.reject(new Error(resp.statusText));
};

const GETIND = function(resp) {
    let dolar = resp.dolar;
    let euro = resp.euro;
    let tasa_desempleo = resp.tasa_desempleo;
    return { dolar, euro, tasa_desempleo };
};

const SAVE = function(resp) {
    filesM.FILEWRITE(resp);
};


function UPDATE() {
    return fetch('https://mindicador.cl/api')
        .then(STATUS)
        .then(GETJSON)
        .then(GETIND)
        .then(SAVE)
        .catch(ERRORS);
}

let ERRORS = function(error) {
    console.log("Error al actualizar los indicadores: " + error);

};

module.exports = {
    UPDATE
};