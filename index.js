var datos = require("./libraries/getValuesInd");
var calculos = require("./calculations/calculation");
var fm = require("./filesmanagement/filesM");

const readline = require('readline');

var sc = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

function MenuCallBack() {


    console.log('--------------------Menú-------------------');
    console.log('1. Actualizar datos');
    console.log('PROM datos');
    console.log('\ta) Dolar');
    console.log('\tb) Euro');
    console.log('\tc) Tasa de desempleo');

    console.log('Valor más actual');
    console.log('\td) Dolar');
    console.log('\te) Euro');
    console.log('\tf) Tasa de desempleo');

    console.log('Mostrar mínimos históricos');
    console.log('\tg) Dolar');
    console.log('\th) Euro');
    console.log('\ti) Tasa de desempleo');

    console.log('Mostrar máximos históricos');
    console.log('\tj) Dolar');
    console.log('\tk) Euro');
    console.log('\tl) Tasa de desempleo');

    console.log('0. Salir');
    console.log('Elija una opción ');
    sc.question('', opcion => {
        switch (opcion) {
            case '1':
                datos.UPDATE();
                MenuCallBack();
                break;

            case 'a':
                calculos.PROM('dolar');
                MenuCallBack();
                break;
            case 'b':
                calculos.PROM('euro');
                MenuCallBack();
                break;
            case 'c':
                calculos.PROM('tasa_desempleo');
                MenuCallBack();
                break;

            case 'd':
                fm.UPDATED('dolar');
                MenuCallBack();
                break;
            case 'e':
                fm.UPDATED('euro');
                MenuCallBack();
                break;
            case 'f':
                fm.UPDATED('tasa_desempleo');
                MenuCallBack();
                break;

            case 'g':
                calculos.MIN('dolar');
                MenuCallBack();
                break;
            case 'h':
                calculos.MIN('euro');
                MenuCallBack();
                break;
            case 'i':
                calculos.MIN('tasa_desempleo');
                MenuCallBack();
                break;

            case 'j':
                calculos.MAX('dolar');
                MenuCallBack();
                break;
            case 'k':
                calculos.MAX('euro');
                MenuCallBack();
                break;
            case 'l':
                calculos.MAX('tasa_desempleo');
                MenuCallBack();
                break;

            case '0':
                console.log('Hasta la Proxima!');
                sc.close();
                process.exit(0);
                break;
            default:
                console.log('[Error] opcion invalida');
                MenuCallBack();
                break;
        }
    });
}

MenuCallBack();