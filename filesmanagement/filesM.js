let fs = require('fs');
let path = require('path');

//guarda en archivo
let FILEWRITE = (values) => {
    fs.writeFile('./indicators/' + Date.now() + '.ind',
        JSON.stringify(values, null, "\t"), (error) => {
            if (error) throw error;
        });
};
//obtiene el indicador actualizado
let UPDATED = (i) => {
    fs.readdir('Indicators', (error, objects) => {
        if (error) { console.log(error); } else if (objects.length > 1) {
            let ind = 0;
            for (const e of objects) {
                let num = Number(e.slice(0, e.length - 4));
                if (ind < num) {
                    ind = num;
                }
            }
            //invoca metodo de lectura
            READFILE(ind, i);
        } else {
            console.log("Primero debe actualizar datos");

        }

    });
};

let LIST = () => {
    let files = fs.readdirSync('indicators');
    let filteredList = files.filter((e) => {
        return path.extname(e).toLowerCase() === '.ind';
    });
    return Promise.resolve(filteredList);
};

//obtiene todos los valores del archivo
let getAllFileValues = (i) => {
    return LIST()
        .then(function(data) {
            let values = data.map(function(element) {
                return JSON.parse(fs.readFileSync('indicators' + '/' + element, 'utf-8'))[i];
            });
            return values;
        });
};

let READFILE = (ind, i) => {
    let path = 'Indicators/' + ind + '.ind';
    fs.readFile(path, 'utf-8', (error, data) => {
        if (error) {
            console.log('error: ', err);
        } else {
            let values = JSON.parse(data)[i];
            let fecha = new Date(ind).toLocaleString();
            console.log('indicador actualizado: ' + values.nombre + ':' + values.valor);
            console.log('Los datos fueron extraídos: ' + fecha);
        }
    });
};

module.exports = {
    UPDATED,
    FILEWRITE,
    getAllFileValues
};